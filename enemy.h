/*
 * enemy.h
 *
 *  Created on: Feb 3, 2020
 *      Author: Zivlak
 */

#ifndef ENEMY_H_
#define ENEMY_H_

#include <random>
#include <Windows.h>
#include <time.h>
#include "destroyablewall.h"
#include "tank.h"
#include "projectile.h"

class Enemy : public Tank{
public:
	enum Color : short int { YELLOW = 0, GREEN = 1, PINK = 2};
	short int color;
	short int state;
	int startMove = 0;
	int accumulator = 0;
	int endMove;
	int enemyValue = 0;
	SDL_Rect *src;
	SDL_Rect *dest;
	vector<Projectile*> projectiles;
	Enemy(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	void move();
	void move(int dx, int dy);
	void draw(SDL_Renderer *renderer);
	void drawFrame(SDL_Rect *src, SDL_Rect *dest, SDL_Renderer *renderer);
	bool collision(DestroyableWall *dw);
	bool collisionWithEnemy(SDL_Rect *dest);
	bool hitByTank(vector<Projectile*>);
	void shoot(SDL_Renderer *renderer);
	int randomMove();

	virtual ~Enemy();
};

#endif /* ENEMY_H_ */
