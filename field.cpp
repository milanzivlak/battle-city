/*
 * field.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: Zivlak
 */

#include "field.h"

Field::Field(vector<DestroyableWall*> destroyablewalls) {
	// TODO Auto-generated constructor stub
	this->destroyablewalls = destroyablewalls;

}

void Field::createDestroyables(istream &input, SDL_Renderer *renderer){
	/*
	 * Funkcija zaduzena za kreiranje zidova koji mogu da se uniste.
	 * Koordinate zidova se popunjavaju preko fajla u kojem se nalaze iste.
	 * Takodje, kreira se eksplozija koja ce se iscrtavati prilikom unistavanja zidova.
	 */
	explosion = new DestroyableWall(32, 32, "resources/tilesets/explosions.png", renderer);
	explosion->dest->x = 1000;
	explosion->src->x = 1000;


	while (!input.eof()){
		DestroyableWall *dw = new DestroyableWall(32, 32, "resources/tilesets/destroyableWall.png", renderer);
		input >> dw->dest->x >> dw->dest->y;
		destroyablewalls.push_back(dw);
	}


}
bool Field::isDWCoordinates(vector<DestroyableWall*> destroyables, int x, int y){
	for(DestroyableWall *dw : destroyables){
		SDL_Rect *destDW = dw->dest;

			if((x < destDW->x + destDW->w+2) and (x + destDW->w+2 > destDW->x) and
				(y < destDW->y+destDW->h+2) and (y + destDW->h+2 > destDW->y)){
				return true;
			}
	}
	return false;
}
void Field::createEnemies(SDL_Renderer *renderer){


	vector<int> coordinates;
	coordinates = enemiesCoordinates();

	if(isDWCoordinates(destroyablewalls, coordinates[0], coordinates[1]) == false){
		createdEnemies++;
		Enemy *enemy = new Enemy(32, 32, "resources/creatures/player.png", renderer);
		enemy->dest->x = coordinates[0];
		enemy->dest->y = coordinates[1];
		coordinates.clear();
		enemies.push_back(enemy);
	}

}

void Field::enemyCollision(){
	for(unsigned int i = 0; i < enemies.size(); i++){
		for(unsigned int j = 1; j < enemies.size()-1; j++){
			if(enemies[i]->collisionWithEnemy(enemies[j]->dest)){
				enemies[i]->state = 0;
				enemies[j]->state = 0;
			}
		}
	}

}
void Field::draw(SDL_Renderer *renderer){
	/*
	 * Draw kod field-a je uradjen tako da se prolazi kroz vektore zidova i enemies-a i pozivamo njigove draw metode.
	 */

	for(DestroyableWall *d : destroyablewalls){
		d->draw(renderer);
	}
	for(Enemy *e: enemies){
		e->draw(renderer);
	}
	SDL_RenderCopy(renderer, explosion->texture, explosion->src, explosion->dest);
}

bool Field::tankWithDestroyables(Tank *tank){
	/*
	 * Ispitivanje kolizije tenka sa svim zidovima.
	 * Ukoliko se desi kolizija, ispitujemo njegovo sljedece kretanje.
	 */

	//TODO: Kolizija nije u potpunosti ispravna, vrsi pomjeranje tank-dest za po 1 piksel.

	for(DestroyableWall *dw: destroyablewalls){
		if(tank->collision(dw)){
			return true;
			}
	}
	return false;
}

void Field::enemyWithDestroyables(vector<Enemy*> enemies){
	/*
	 * Ispitivanje kolizije enemy-a sa svim zidovima.
	 */

	//TODO: Kolizija ne radi iz nekog razloga.
	for(Enemy* enemy : enemies){
		for(DestroyableWall *dw: destroyablewalls){
			if(enemy->collision(dw)){
				enemy->state = 0;
			}
		}
	}
}
bool Field::projectileWithDestroyables(vector<Projectile*> projectiles){
	/*
	 * Provjera kolizije projektila sa zidovima koji se mogu unistiti.
	 * Zid se unistava nakon trece kolizije sa projektilom.
	 * Svaki zid ima atribut hits koji prati koliko puta je pogodjen.
	 * Prilikom kolizije sa zidom, pojavljuje se eksplozija.
	 */

	explosionEnd = SDL_GetTicks();
	int diffTimeEx = explosionEnd - explosionStart;
	explosionAccumulator += diffTimeEx;

	if(accumulator > 1000){
		accumulator -= 1000;
		explosion->dest->x = 1000;
	}

	explosionStart = explosionEnd;

	for(Projectile *p : projectiles){
		int counter = 0;

		for(DestroyableWall *dw: destroyablewalls){

			if(p->dWallCollision(dw)){
				explosion->dest->x = p->dest->x;
				explosion->dest->y = p->dest->y;
				if( dw->hits == 2){
					explosion->dest->x = dw->dest->x;
					explosion->dest->y = dw->dest->y;
					dw->hits = 0;

					destroyablewalls.erase(destroyablewalls.begin() + counter);
				}else{
					dw->hits++;
				}

				return true;
			}else{

				counter++;
			}

		}


	}
	return false;
}
bool Field::projectileWithEnemies(vector<Projectile*> projectiles){
	/*
	 * Ispitivanje kolizije projektila sa enemy-em.
	 * Ukoliko se desi, enemy-a uklanjamo iz vektora i vracamo true.
	 */
	end = SDL_GetTicks();
	int diffTime = end - start;
	accumulator += diffTime;

	if(accumulator > 2000){
		accumulator -= 2000;
		score->dest->x = 1000;
	}

	start = end;

	for(Projectile *p:projectiles){
		int counter = 0;

		for(Enemy *e : enemies){
			SDL_Rect *destDW = e->dest;

			if((p->dest->x < destDW->x + destDW->w) and (p->dest->x + p->dest->w > destDW->x) and
				(p->dest->y < destDW->y+destDW->h) and (p->dest->y + p->dest->h > destDW->y)){
				explosion->dest->x = destDW->x;
				explosion->dest->y = destDW->y;
				tankScore += e->enemyValue;
				score->tableScore = e->enemyValue;
				score->dest->x = e->dest->x;
				score->dest->y = e->dest->y;
				enemies.erase(enemies.begin() + counter);
				return true;
			}else{
				counter++;
			}
		}
	}
	return false;
}
void Field::moveEnemies(){
	/*
	 * Prolazimo kroz sve enemies-e i pozivamo njihov move koji je uradjen preko random brojeva.
	 */

	for(Enemy *e : enemies){
		e->move();
	}
}

vector<int> Field::enemiesCoordinates(){
	/*
	 * Funkcija namijenjena random izboru koordinata enemy-a.
	 * Koordinate x i y se dodaju u vektor gdje kasnije indeksom 0 i 1 dobijamo iste, nakon toga se vektor clear-uje.
	 * Prilikom kreiranja svakog, uzimamo nove random koordinate i koristimo za njegov dest.
	 */

	vector<int> coordinates;

	srand (time(NULL));

	int x = rand() % 578 + 0;
	int y = rand() % 448 + 0;

	coordinates.push_back(x);
	coordinates.push_back(y);

	return coordinates;
}

void Field::enemyShooting(vector<Enemy*> enemies, SDL_Renderer *renderer){
	/*
	 * Funkcija koja se koristi za pozivanje metode shoot().
	 * Uzmemo jedan random broj iz opsega 0 - duzina vektora enemies-a.
	 * Kad dobijemo random broj, pozivamo metodu shoot za enemy-a koji se nalazi na tom indeksu.
	 */

	int randomEnemy = rand() % enemies.size() + 0;
	enemies[randomEnemy]->shoot(renderer);



}

void Field::smartShooting(vector<Enemy*> enemies, Tank *tank, SDL_Renderer *renderer){
	/*
	 * Ispitujemo vidno polje enemy-a.
	 * Kreiramo jedan pravougaonik koji predstavlja njegovo vidno polje.
	 * U zavisnosti od njegovog steta, rotiramo ga u tom smjeru i ispitujemo da li se tenk nalazi unutar toga.
	 * Ukoliko se tenk nalazi unutar toga, svake 2 sekunde pozivamo metodu shoot().
	 */
	SDL_Rect *rect = new SDL_Rect();
	rect->x = 0;
	rect->y = 0;
	rect->w = 0;
	rect->h = 0;

	for(Enemy *enemy : enemies){


		if(enemy->state == 1){
			rect->x = enemy->dest->x-300;
			rect->y = enemy->dest->y;
			rect->h = enemy->dest->h;
			rect->w = enemy->dest->w+300;

			if((tank->dest->x < rect->x + rect->w) and (tank->dest->x + tank->dest->w > rect->x) and
					(tank->dest->y < rect->y+rect->h) and (tank->dest->y + tank->dest->h > rect->y)){
					endShooting = SDL_GetTicks();
					int diff = endShooting - startShooting;
					shootingAcc += diff;

					if(shootingAcc > 2000){
						shootingAcc -= 2000;
						enemy->shoot(renderer);
					}

					startShooting = endShooting;

					rect->w = 0;
			}


		}else if(enemy->state == 2){
			rect->x = enemy->dest->x;
			rect->y = enemy->dest->y;
			rect->h = enemy->dest->h;
			rect->w = enemy->dest->w+300;

			if((tank->dest->x < rect->x + rect->w) and (tank->dest->x + tank->dest->w+2 > rect->x) and
					(tank->dest->y < rect->y+rect->h) and (tank->dest->y + tank->dest->h+2 > rect->y)){

					endShooting = SDL_GetTicks();
					int diff = endShooting - startShooting;
					shootingAcc += diff;

					if(shootingAcc > 2000){
						shootingAcc -= 2000;
						enemy->shoot(renderer);
					}

					startShooting = endShooting;

					rect->w = 0;
			}
		}else if(enemy->state == 4){
			rect->x = enemy->dest->x;
			rect->y = enemy->dest->y-300;
			rect->h = enemy->dest->h+300;
			rect->w = enemy->dest->w;

			if((tank->dest->x < rect->x + rect->w) and (tank->dest->x + tank->dest->w+2 > rect->x) and
					(tank->dest->y < rect->y+rect->h) and (tank->dest->y + tank->dest->h+2 > rect->y)){

					endShooting = SDL_GetTicks();
					int diff = endShooting - startShooting;
					shootingAcc += diff;

					if(shootingAcc > 2000){
						shootingAcc -= 2000;
						enemy->shoot(renderer);
					}

					startShooting = endShooting;

					rect->w = 0;
			}
		}else if(enemy->state == 8){
			rect->x = enemy->dest->x;
			rect->y = enemy->dest->y;
			rect->h = enemy->dest->h+300;
			rect->w = enemy->dest->w;

			if((tank->dest->x < rect->x + rect->w) and (tank->dest->x + tank->dest->w+2 > rect->x) and
					(tank->dest->y < rect->y+rect->h) and (tank->dest->y + tank->dest->h+2 > rect->y)){

					endShooting = SDL_GetTicks();
					int diff = endShooting - startShooting;
					shootingAcc += diff;

					if(shootingAcc > 2000){
						shootingAcc -= 2000;
						enemy->shoot(renderer);
					}

					startShooting = endShooting;

					rect->w = 0;
			}
		}
	}
}

bool Field::tankHitted(Tank *tank, vector<Projectile*> projectiles){
	for(Projectile *p : projectiles){
		if(tank->collisionWithProjectile(p)){
			return true;
		}
	}
	return false;
}

bool Field::projectileWithBase(Base *base, vector<Projectile*> projectiles){
	for(Projectile *p : projectiles){
		SDL_Rect *destDW = p->dest;

		if((base->dest->x < destDW->x + destDW->w+2) and (base->dest->x + base->dest->w+2 > destDW->x) and
			(base->dest->y < destDW->y+destDW->h+2) and (base->dest->y + base->dest->h+2 > destDW->y)){
			return true;
		}
	}
	return false;
}

void Field::drawScore(int score, TTF_Font* font, SDL_Renderer* renderer){

	SDL_Color white = {255, 255, 255};
	stringstream ss;
	stringstream ss2;
	int enemyNumber = 8 - createdEnemies;
	ss << "Score: " << score;
	ss2 << "Enemies : " << enemyNumber;

	//Prva poruka
	SDL_Surface* sm = TTF_RenderText_Solid(font, ss.str().c_str(), white);
	SDL_Texture* poruka = SDL_CreateTextureFromSurface(renderer, sm);
	SDL_Rect poruka_box;
	poruka_box.x = 625;
	poruka_box.y = 5;
	poruka_box.w = sm->w;
	poruka_box.h = sm->h;
	SDL_RenderCopy(renderer, poruka, NULL, &poruka_box);

	//Druga poruka
	SDL_Surface* sm2 = TTF_RenderText_Solid(font, ss2.str().c_str(), white);
	SDL_Texture* poruka2 = SDL_CreateTextureFromSurface(renderer, sm2);
	SDL_Rect poruka_box2;
	poruka_box2.x = 625;
	poruka_box2.y = 25;
	poruka_box2.w = sm->w;
	poruka_box2.h = sm->h;
	SDL_RenderCopy(renderer, poruka2, NULL, &poruka_box2);

}

void Field::drawEnd(int tankX, int tankY, TTF_Font *font, SDL_Renderer *renderer){
	SDL_Color white = {255, 255, 255};
	stringstream ss;
	ss << "GAME OVER";

	//Prva poruka
	SDL_Surface* sm = TTF_RenderText_Solid(font, ss.str().c_str(), white);
	SDL_Texture* poruka = SDL_CreateTextureFromSurface(renderer, sm);
	SDL_Rect poruka_box;
	poruka_box.x = tankX+50;
	poruka_box.y = tankY+50;
	poruka_box.w = sm->w;
	poruka_box.h = sm->h;
	SDL_RenderCopy(renderer, poruka, NULL, &poruka_box);
}
Field::~Field() {
	// TODO Auto-generated destructor stub
}

