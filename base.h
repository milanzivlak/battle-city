/*
 * base.h
 *
 *  Created on: Jan 23, 2020
 *      Author: Zivlak
 */

#ifndef BASE_H_
#define BASE_H_
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "tank.h"
#include "drawable.h"
#include "projectile.h"
using namespace std;
class Base : public Drawable{
public:
	SDL_Rect *src;
	SDL_Rect *dest;
	SDL_Texture *texture;
	int width;
	int height;
	Base(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	void draw(SDL_Renderer *renderer);
	bool collisionWithTank(Tank *tank);
	bool collisionWithProjectile(Projectile *projectile);
	virtual ~Base();
};

#endif /* BASE_H_ */
