/*
 * table.cpp
 *
 *  Created on: Mar 16, 2020
 *      Author: Zivlak
 */

#include "table.h"

Table::Table(int width, int height, string spriteSheetPath, SDL_Renderer *renderer) {
	// TODO Auto-generated constructor stub
	dest = new SDL_Rect();
	dest->x = 610;
	dest->y = 0;
	dest->w = width;
	dest->h = height;

	src = new SDL_Rect();
	src->x = 0;
	src->y = 0;
	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);




}

void Table::draw(SDL_Renderer *renderer){
	SDL_RenderCopy(renderer, texture, src, dest);
}


Table::~Table() {
	// TODO Auto-generated destructor stub
}

