#include "engine.h"

ostream& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}

Engine::Engine(string title) {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, 720, 480, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Engine::addTileset(Tileset *tileset, const string &name) {
    tilesets[name] = tileset;
}

void Engine::addTileset(istream &inputStream, const string &name) {
    addTileset(new Tileset(inputStream, renderer), name);
}

void Engine::addTileset(const string &path, const string &name) {
    ifstream tilesetStream(path);
    addTileset(tilesetStream, name);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addDrawable(Drawable *drawable) {
    drawables.push_back(drawable);
}

void Engine::run() {
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    bool running = true;
    SDL_Event event;

    cout << (*dynamic_cast<Level*>(drawables[0])) << endl;



    //Inicijalizacija truetype fonta
    int ret = TTF_Init();
    TTF_Font* font = TTF_OpenFont("Adigiana.ttf", 20);
    //Tank

    ifstream destroyableCoordinates("resources/tilesets/destroyables.txt");
    Tank *tank = new Tank(32, 32, "resources/creatures/player.png", renderer);
    drawables.push_back(tank);
    vector<Projectile*> projectiles;
    tank->projectiles = projectiles;
    DestroyableWall *baseWall = new DestroyableWall(64, 40, "resources/tilesets/baseWall.png", renderer);
    Table *table = new Table(110, 530, "resources/tilesets/table.png", renderer);
    Score *score = new Score(16, 13, "resources/tilesets/scores.png", renderer);

    vector<DestroyableWall*> destroyablewalls;
    vector<Enemy*> enemies;
    Field *field = new Field(destroyablewalls);
    field->score = score;
    field->destroyablewalls.push_back(baseWall);
    field->createDestroyables(destroyableCoordinates, renderer);
    field->enemies = enemies;


    cout << field->enemies.size() << endl;
    baseWall->dest->x = 264;
    baseWall->dest->y = 442;


    Base *base = new Base(32, 32, "resources/tilesets/base.png", renderer);

    movables.push_back(tank);
    drawables.push_back(field);
    drawables.push_back(table);
    drawables.push_back(score);
    drawables.push_back(base);
    eventListeners.push_back(tank);


    while(running) {
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
            	delete field;
            	tank->~Tank();
            	delete table;
            	delete score;
                running = false;
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++) {
                    eventListeners[i]->listen(event);

                }
            }
        }

        //Kreiranje enemies-a
        /*
         * Ispitujemo koliko je enemy-a kreirano, ako je 0, da odmah na pocetku kreira.
         * Da sam ispitivao za vektor enemy-a, on bi kreirao sledeceg istog momenta kad bi taj jedini bio unisten i izbacen iz vektora.
         */

        if(field->createdEnemies == 0){
			field->createEnemies(renderer);
		}
		if(field->createdEnemies<8){

				end = SDL_GetTicks();
				int diffTime = end - start;
				accumulator += diffTime;

				if(accumulator > 5000){
					accumulator -= 5000;
					field->createEnemies(renderer);
				}

				start = end;
			}


        //Ispitujemo koliziju projektila sa unistivim zidovima ili enemy-em i te projektile izbacujemo iz vektora.
        int projectileCounter = 0;
        if(field->projectileWithDestroyables(tank->projectiles) or field->projectileWithEnemies(tank->projectiles)){

        	tank->projectiles.erase(tank->projectiles.begin()  + projectileCounter);

        }else{
        	projectileCounter++;

        }

        //Prolazimo kroz enemies-e i ispitujemo da li je doslo do kolizije izmedju
        //njihovih projektila i zidova koji mogu da se uniste.
        if(field->enemies.size() != 0){
        	for(Enemy* e : field->enemies){
				projectileCounter = 0;
				if(field->projectileWithDestroyables(e->projectiles)){
					e->projectiles.erase(e->projectiles.begin() + projectileCounter);
				}else{
					projectileCounter++;
				}
			}
        }


		//Svake sekunde se poziva metoda enemyShooting za sve enemies-e
//		endShooting = SDL_GetTicks();
//		int diff = endShooting - startShooting;
//		shootingAccumulator += diff;
//
//		if(shootingAccumulator > 1000){
//			shootingAccumulator -= 1000;
//			if(field->enemies.size() != 0){
//				field->enemyShooting(field->enemies, renderer);
//			}
//
//		}
//
//		startShooting = endShooting;

		//AI shooting


		if(field->enemies.size() != 0){
			field->moveEnemies();

		}


        //Ispitivanje da li je doslo do kolizije tenka sa projektilom od nekog enemy-a


        for(size_t i = 0; i < movables.size(); i++) {
        	field->enemyWithDestroyables(field->enemies);
        	field->enemyCollision();

        	//Ukoliko se desila kolizija tenka sa unistivim zidovima ili zidom od baze, njegov state je 0, odnosno STOP.
			if(field->tankWithDestroyables(tank) or tank->collision(baseWall) or base->collisionWithTank(tank)){
				tank->state = 0;
			}
        	movables[i]->move();
        }

        if(field->enemies.size() == 0 and field->createdEnemies == 8){
        	cout << "Congratulations, you have won!" << endl;
        	drawables.erase(drawables.begin() + 1);
        	tank->~Tank();

        }
        //Provjeravamo da li je doslo do kolizije tenka ili baze sa projektilima.
        //Ako jeste, igrica je zavrsena.
        if(field->enemies.size() != 0){
        	for(Enemy *e : field->enemies){
				if(field->tankHitted(tank, e->projectiles)){
					drawables.erase(drawables.begin()+1);
					//field->drawEnd(tank->dest->x, tank->dest->y, font, renderer);
					tank->~Tank();
				}else if(field->projectileWithBase(base, e->projectiles)){
					//field->destroyablewalls.erase(field->destroyablewalls.begin() + 1);
					//field->drawEnd(tank->dest->x, tank->dest->y, font, renderer);
					//delete tank;
					tank->~Tank();
				}

			}
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++) {
        	field->drawScore(field->tankScore, font, renderer);
        	field->smartShooting(field->enemies, tank, renderer);
            drawables[i]->draw(renderer);
        }

        SDL_RenderPresent(renderer);
        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay) {
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }
    }
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
