/*
 * Tank.cpp
 *
 *  Created on: Dec 26, 2019
 *      Author: Zivlak
 */

#include "tank.h"

Tank::Tank(int width, int height, string spriteSheetPath, SDL_Renderer *renderer) : Drawable(), Movable(), KeyboardEventListener(){
	state = STOP;
	dest = new SDL_Rect();
	dest->x = 200;
	dest->y = 200;
	dest->w = width;
	dest->h = height;
	nextX = dest->x;
	nextY = dest->y;

	src = new SDL_Rect();
	src->x = 0;
	src->y = 0;
	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
}
Tank::Tank(istream &inputStream){
	int x, y, w, h;
	inputStream >> x >> y >> w >> h;
	src = new SDL_Rect;
	src->x = x;
	src->y = y;
	src->w = w;
	src->h = h;
}
void Tank::draw(SDL_Renderer *renderer){
	noviRend = renderer;
	//Current state
	if(state&LEFT) {
		src->x = 72;
		lastState = LEFT;
		this->drawFrame(src, dest, renderer);
	} else if(state&RIGHT) {
		lastState = RIGHT;
		src->x = 206;
		this->drawFrame(src, dest, renderer);
	} else if(state&UP) {
		lastState = UP;
		src->x = 0;
		this->drawFrame(src, dest, renderer);
	} else if(state&DOWN) {
		lastState = DOWN;
		src->x = 138;
		this->drawFrame(src, dest, renderer);
	}else if(state&SHOOT){
		this->shoot(renderer);

	}else{
		if(lastState&LEFT){
			src->x = 72;
		}else if(lastState&RIGHT){
			src->x = 206;
		}else if(lastState&UP){
			src->x = 0;
		}else if(lastState&DOWN){
			src->x = 138;
		}

		this->drawFrame(src, dest, renderer);
	}

	//Last state
	if(lastState&LEFT) {
		src->x = 72;
		this->drawFrame(src, dest, renderer);
	} else if(lastState&RIGHT) {
		src->x = 206;
		this->drawFrame(src, dest, renderer);
	} else if(lastState&UP) {
		src->x = 0;
		this->drawFrame(src, dest, renderer);
	} else if(lastState&DOWN) {
		src->x = 138;
		this->drawFrame(src, dest, renderer);
	}

	for(Projectile *p : projectiles){
		p->move();
		p->draw(renderer);
	}

}


void Tank::drawFrame(SDL_Rect *src, SDL_Rect *dest, SDL_Renderer *renderer){

    SDL_RenderCopy(renderer, texture, src, dest);

}
void Tank::move(int dx, int dy) {

    dest->x += dx;
    dest->y += dy;
    nextX = dest->x;
    nextY = dest->y;


    //Ogranicavanje kretanja van ekrana.
    if(dest->x <= 0){
    	dest->x = 0;
    }
    if(dest->x >= 578){
    	dest->x = 578;
    }
    if(dest->y <= 0){
    	dest->y = 0;
    }
    if(dest->y >= 448){
    	dest->y = 448;
    }
	//cout << "X: " << this->dest->x << ", Y: " << this->dest->y << " X+W: " << this->dest->x+this->dest->w << ", Y+H: " << this->dest->y+this->dest->w<< endl;



}

void Tank::move() {
    if(state != 0) {
        if(state & 1) {
            move(-1, 0);
            //last = 1;

        }
        if(state & 2) {
            move(1, 0);
            //last = 2;
        }
        if(state & 4) {
            move(0, -1);
            //last = 4;
        }
        if(state & 8) {
            move(0, 1);
            //last = 8;
        }
        if( state&10 ){
        	move(0,0);
        }
        if((state & 1) and (state & 4)){
        	move(0,0);
        }
        if((state & 1) and (state & 8)){
        	move(0,0);
        }
        if((state & 2) and (state & 4)){
			move(0,0);
		}
        if((state & 2) and (state & 8)){
			move(0,0);
        }
    }else if( state&10){
    	move(0,0);

    }else{
    	move(0,0);
    }
    last = state;
  }


void Tank::listen(SDL_KeyboardEvent &event) {
	/*
	 * Kod listen-a, povecavamo i smanjujemo njegove nextX i nextY koordinate koje kasnije koristimo za koliziju.
	 */

    if(event.type == SDL_KEYDOWN) {
        if(event.keysym.sym == SDLK_LEFT) {
        	state = LEFT;
        	nextState = LEFT;
        	nextX--;
        } else if(event.keysym.sym == SDLK_RIGHT) {
        	state = RIGHT;
        	nextState = RIGHT;
        	nextX++;
        } else if(event.keysym.sym == SDLK_UP) {
        	state = UP;
        	nextState = UP;
        	nextY--;
        } else if(event.keysym.sym == SDLK_DOWN) {
        	state = DOWN;
        	nextState = DOWN;
        	nextY++;
        }else if(event.keysym.sym == SDLK_SPACE){

			this->shoot(noviRend);
        }
    }else{
    	state = STOP;
    }

}

bool Tank::collision(DestroyableWall *dw){
	/*
	 * Ispitujemo trenutnu i buducu koliziju tenka sa unistivim zidom.
	 */

	SDL_Rect *destDW = dw->dest;

	if((dest->x < destDW->x + destDW->w+2) and (dest->x + dest->w+2 > destDW->x) and
		(dest->y < destDW->y+destDW->h+2) and (dest->y + dest->h+2 > destDW->y) and
	 state == lastState){

		if(state == 1){
			if(dest->x == destDW->x+1 + destDW->w){
				state = 0;
				return true;
			}
		}else if(state == 2){
			if(dest->x+1 + dest->w == destDW->x){
				return true;
			}
		}else if(state == 4){
			if(dest->y == destDW->y + destDW->h){
				return true;
			}
		}else if(state == 8){
			if(dest->y + dest->h == destDW->y){
				return true;
			}
		}
	}
	return false;
}

bool Tank::collisionWithProjectile(Projectile *p){
	SDL_Rect *destDW = p->dest;

	if((dest->x < destDW->x + destDW->w) and (dest->x + dest->w > destDW->x) and
		(dest->y < destDW->y+destDW->h) and (dest->y + dest->h > destDW->y)){
		return true;
	}
	return false;
}

void Tank::shoot(SDL_Renderer *renderer){
	/*
	 * Ispaljivanje se vrsi tako sto se projektili kreiraju i ispaljuju u onom smjeru u kojem se tenk krece,
	 * odnosno, koji mu je trenutni state.
	 * Projektili se dodaju u vektor kroz koji se prolazi da bi se iscrtali isti.
	 */

	if( lastState == LEFT ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);

		projectile1->dest->x = dest->x-10;
		projectile1->dest->y = dest->y;
		projectile1->state = LEFT;
		projectiles.push_back(projectile1);

	}else if( lastState == RIGHT ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+30;
		projectile1->dest->y = dest->y;
		projectile1->state = RIGHT;
		projectiles.push_back(projectile1);

	}else if( lastState == UP ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+8;
		projectile1->dest->y = dest->y-24;
		projectile1->state = UP;
		projectiles.push_back(projectile1);

	}else if( lastState == DOWN ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+8;
		projectile1->dest->y = dest->y+20;
		projectile1->state = DOWN;
		projectiles.push_back(projectile1);
	}

}
Tank::~Tank() {
	// TODO Auto-generated destructor stub
	delete src;
	delete dest;
}

