/*
 * projectile.h
 *
 *  Created on: Jan 25, 2020
 *      Author: Zivlak
 */

#ifndef PROJECTILE_H_
#define PROJECTILE_H_
#include "tank.h"
#include "drawable.h"
#include "movable.h"
#include <SDL2/SDL.h>
using namespace std;

class Projectile : public Drawable, public Movable{
public:
	enum State:short int {STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8, SHOOT = 10,
		                     LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
		                     RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};
	int width;
	int height;
	SDL_Rect *src;
	SDL_Rect *dest;
	SDL_Texture *texture;
	short int state;
	Projectile(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	virtual void draw(SDL_Renderer *renderer);
	virtual void move(int dx, int dy);
	virtual void move();
	bool dWallCollision(DestroyableWall *dw);
	virtual ~Projectile();
};

#endif /* PROJECTILE_H_ */
