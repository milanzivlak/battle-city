/*
 * table.h
 *
 *  Created on: Mar 16, 2020
 *      Author: Zivlak
 */

#ifndef TABLE_H_
#define TABLE_H_
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "score.h"
#include <iostream>
using namespace std;
//class Score{
//	Score(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
//}
class Table : public Drawable{

public:

	Score *score;
	SDL_Rect *src;
	SDL_Rect *dest;
	SDL_Texture *texture;
	int width;
	int height;
	void draw(SDL_Renderer * renderer);
	Table(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	virtual ~Table();
};

#endif /* TABLE_H_ */
