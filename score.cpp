/*
 * score.cpp
 *
 *  Created on: Mar 16, 2020
 *      Author: Zivlak
 */

#include "score.h"

Score::Score(int width, int height, string spriteSheetPath, SDL_Renderer *renderer) {
	// TODO Auto-generated constructor stub
	dest = new SDL_Rect();
	dest->x = 1650;
	dest->y = 30;
	dest->w = width;
	dest->h = height;

	src = new SDL_Rect();


	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

}

void Score::draw(SDL_Renderer *renderer){
	if(tableScore == 100){
		src->x = 0;
		src->y = 0;
	}else if(tableScore == 200){
		src->x = 16;
		src->y = 0;
	}else if(tableScore == 300){
		src->x = 32;
		src->y = 0;
	}
	SDL_RenderCopy(renderer, texture, src, dest);
}
Score::~Score() {
	// TODO Auto-generated destructor stub
}

