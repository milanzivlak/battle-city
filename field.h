/*
 * field.h
 *
 *  Created on: Jan 26, 2020
 *      Author: Zivlak
 */

#ifndef FIELD_H_
#define FIELD_H_
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "drawable.h"
#include "destroyablewall.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "tank.h"
#include "projectile.h"
#include <vector>
#include "base.h"
#include "enemy.h"
#include "score.h"
using namespace std;
class Field : public Drawable{
public:

	vector<DestroyableWall*> destroyablewalls;
	vector<Enemy*> enemies;
	DestroyableWall *explosion;
	Score *score;
	int start = 0;
	int explosionStart = 0;
	int explosionEnd;
	int explosionAccumulator;
	int end;
	int accumulator = 0;
	int createdEnemies = 0;
	int tankScore = 0;
	int startShooting = 0;
	int endShooting;
	int shootingAcc = 0;
	Field(vector<DestroyableWall*>);
	void createDestroyables(istream &input, SDL_Renderer *renderer);
	vector<int> enemiesCoordinates();
	bool isDWCoordinates(vector<DestroyableWall*>, int x, int y);
	void createEnemies(SDL_Renderer *renderer);
	void draw(SDL_Renderer *renderer);
	bool tankWithDestroyables(Tank *tank);
	void enemyWithDestroyables(vector<Enemy*>);
	bool projectileWithDestroyables(vector<Projectile*>);
	bool projectileWithEnemies(vector<Projectile*>);
	void moveEnemies();
	void enemyCollision();
	void enemyShooting(vector<Enemy*>, SDL_Renderer *renderer);
	void smartShooting(vector<Enemy*>, Tank *tank, SDL_Renderer *renderer);
	bool tankHitted(Tank *tank, vector<Projectile*> projectiles);
	bool projectileWithBase(Base *base, vector<Projectile*> projectiles);
	void drawScore(int score, TTF_Font* font, SDL_Renderer* renderer);
	void drawEnd(int tankX, int tankY, TTF_Font* font, SDL_Renderer* renderer);

	virtual ~Field();
};

#endif /* FIELD_H_ */
