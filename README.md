**BATTLE CITY**

*2D igrica koja predstavlja simulaciju borbe tenkova.
Glavni igrac upravlja sa jednim tenkom, dok sa ostalima upravlja kompjuter.*


Glavni igrac koristi strelice za upravljanje i SPACE za ispaljivanje projektila,
dok kompjuter sa ostalima upravlja preko funkcija koja uzima random brojeve za 
stanja, a ispaljivanje projektila vrsi ukoliko se tenk pojavi u odredjenom
prostoru ispred njega.

U igrici postoji odredjeni field sa zidovima i bazom.
Za sve navedene elemente u igrici postoje kolizije kao sto su:
* kolizija tenkova sa zidovima i krajem ekrana
* kolizija tenkova sa projektilima i njihovo unistavanje
* kolizija projektila sa zidovima i obostrano unistavanje
* kolizija za projektile sa tenkom i bazom (GAME OVER)


***Note**: kolizija tenka sa enemy-em i enemy-a sa enemy-em se iz nekog razloga ne desi, ne znam zasto.*