/*
 * enemy.cpp
 *
 *  Created on: Feb 3, 2020
 *      Author: Zivlak
 */

#include "enemy.h"

Enemy::Enemy(int width, int height, string spriteSheetPath, SDL_Renderer *renderer) : Tank(width, height, spriteSheetPath, renderer) {
	// TODO Auto-generated constructor stub
	int randColor = rand()%3+0;
	color = randColor;
	if(color == 0){
		spriteSheetPath = "resources/creatures/opponentYellow.png";
		enemyValue = 100;
	}else if(color == 1){
		spriteSheetPath = "resources/creatures/opponentGreen.png";
		enemyValue = 200;
	}else if(color == 2){
		spriteSheetPath = "resources/creatures/opponentPink.png";
		enemyValue = 300;
	}
	state = STOP;
	dest = new SDL_Rect();
	dest->x = 10;
	dest->y = 10;
	dest->w = width;
	dest->h = height;
	nextX = dest->x;
	nextY = dest->y;

	src = new SDL_Rect();
	src->x = 0;
	src->y = 0;
	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
}
void Enemy::move(int dx, int dy){
	/*
	 * Funkcija koja vrsi upravljanje kretanjem enemy-a.
	 * Enemy se pomjera tako sto uzima jedan radnom broj iz liste [1,2,4,8] i postavlja mu to za trenutni state.
	 * Nakon sto prodje jedna sekunda, ponovo se uzima random broj.
	 */

	dest->x += dx;
	dest->y += dy;


	endMove = SDL_GetTicks();
	int diffTime = endMove - startMove;
	accumulator += diffTime;

	if(accumulator > 1000){
		accumulator -= 1000;
		state = randomMove();
		if(state == STOP){
			accumulator += 500;
		}

	}
	startMove = endMove;

	if(dest->x <= 0){
		dest->x = 0;
	}else if(dest->x >= 578){
		dest->x = 578;
	}
	if(dest->y <= 0){
		dest->y = 0;

	}else if(dest->y >= 448){
		dest->y = 448;
	}

}
void Enemy::move(){


	if(state != 0) {
		if(state & 1) {
			move(-1, 0);

		}
		if(state & 2) {
			move(1, 0);
		}
		if(state & 4) {
			move(0, -1);
		}
		if(state & 8) {

			move(0, 1);
		}
	}else{
		move(0,0);
	}
}

bool Enemy::collision(DestroyableWall *dw){
	SDL_Rect *destDW = dw->dest;

		if((dest->x < destDW->x + destDW->w+2) and (dest->x + dest->w+2 > destDW->x) and
			(dest->y < destDW->y+destDW->h+2) and (dest->y + dest->h+2 > destDW->y) and
		 state == lastState){



		if(state == 1){
			if(dest->x == destDW->x+destDW->w){
				return false;
			}else{
				return true;
			}
		}else if(state == 2){
			if(dest->x+1+dest->w == destDW->x){
				return true;
			}else{
				return false;
			}
		}else if(state == 4){
			if(dest->y == destDW->y + destDW->h){
				return true;
			}
		}else if(state == 8){
			if(dest->y + dest->h == destDW->y){
				return true;
			}
		}
	}
	return false;
}

bool Enemy::collisionWithEnemy(SDL_Rect *dest){
	if((dest->x < dest->x + dest->w+2) and (dest->x + dest->w+2 > dest->x) and
			(dest->y < dest->y+dest->h+2) and (dest->y + dest->h+2 > dest->y) and
		 state == lastState){



		if(state == 1){
			if(dest->x == dest->x+dest->w){
				return false;
			}else{
				return true;
			}
		}else if(state == 2){
			if(dest->x+1+dest->w == dest->x){
				return true;
			}else{
				return false;
			}
		}else if(state == 4){
			if(dest->y == dest->y + dest->h){
				return true;
			}
		}else if(state == 8){
			if(dest->y + dest->h == dest->y){
				return true;
			}
		}
	}
	return false;
}
int Enemy::randomMove(){
	/*
	 * Funkcija koja je zaduzena za random izbor state-a enemy-a.
	 */
	vector<int> stateList{0, 1, 2, 4, 8};
	int randIndex = rand()%stateList.size(); //Biramo random index iz liste
	int stateValue = stateList[randIndex];

	return stateValue;
}

void Enemy::draw(SDL_Renderer *renderer){
	/*
	 * Draw je osmisljen tako da prati zadnje stanje prije eventualne kolizije.
	 * Ako se desila kolozija dok je isao u desno, dok god stoji iscrtavace ga kao da se krece desno.
	 */

	if(state&LEFT) {
		src->x = 72;
		lastState = LEFT;
		this->drawFrame(src, dest, renderer);
	} else if(state&RIGHT) {
		lastState = RIGHT;
		src->x = 206;
		this->drawFrame(src, dest, renderer);
	} else if(state&UP) {
		lastState = UP;
		src->x = 0;
		this->drawFrame(src, dest, renderer);
	} else if(state&DOWN) {
		lastState = DOWN;
		src->x = 138;
		this->drawFrame(src, dest, renderer);
	}else if(state&SHOOT){
		this->shoot(renderer);

	}else{
		if(lastState&LEFT){
			src->x = 72;
		}else if(lastState&RIGHT){
			src->x = 206;
		}else if(lastState&UP){
			src->x = 0;
		}else if(lastState&DOWN){
			src->x = 138;
		}

		this->drawFrame(src, dest, renderer);
		}

		//Last state
		if(lastState&LEFT) {
			src->x = 72;
			this->drawFrame(src, dest, renderer);
		} else if(lastState&RIGHT) {
			src->x = 206;
			this->drawFrame(src, dest, renderer);
		} else if(lastState&UP) {
			src->x = 0;
			this->drawFrame(src, dest, renderer);
		} else if(lastState&DOWN) {
			src->x = 138;
			this->drawFrame(src, dest, renderer);
		}

		for(Projectile *p : projectiles){
			p->move();
			p->draw(renderer);
		}
}

bool Enemy::hitByTank(vector<Projectile*> projectiles){
	/*
	 * Prolazimo kroz sve projektile i ispitujemo da li su pogodili enemy-a.
	 * Funkcija se poziva za projektile main player-a, odnosno tenka u ovom slucaju.
	 * Ukoliko se desila kolizija, samo cemo enemy-a izbaciti iz vektora enemies-a(to je uradjeno u enginu).
	 */
	for(Projectile *p:projectiles){

		if((this->dest->x < p->dest->x + p->dest->w+2) and (this->dest->x + this->dest->w+2 > p->dest->x) and
			(this->dest->y < p->dest->y+p->dest->h+2) and (this->dest->y + this->dest->h+2 > p->dest->y)){
			return true;
		}
	}

	return false;
}

void Enemy::shoot(SDL_Renderer *renderer){
	if( state == LEFT ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);

		projectile1->dest->x = dest->x-10;
		projectile1->dest->y = dest->y;
		projectile1->state = LEFT;
		projectiles.push_back(projectile1);

	}else if( state == RIGHT ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+30;
		projectile1->dest->y = dest->y;
		projectile1->state = RIGHT;
		projectiles.push_back(projectile1);

	}else if( state == UP ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+8;
		projectile1->dest->y = dest->y-24;
		projectile1->state = UP;
		projectiles.push_back(projectile1);

	}else if( state == DOWN ) {
		Projectile *projectile1 = new Projectile(16, 30, "resources/creatures/projectiles.png", renderer);
		projectile1->dest->x = dest->x+8;
		projectile1->dest->y = dest->y+20;
		projectile1->state = DOWN;
		projectiles.push_back(projectile1);
	}
}
void Enemy::drawFrame(SDL_Rect *src, SDL_Rect *dest, SDL_Renderer *renderer){
	SDL_RenderCopy(renderer, texture, src, dest);
}
Enemy::~Enemy() {
	// TODO Auto-generated destructor stub
	delete dest;
	delete src;
}

