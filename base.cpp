/*
 * base.cpp
 *
 *  Created on: Jan 23, 2020
 *      Author: Zivlak
 */

#include "base.h"

Base::Base(int width, int height, string spriteSheetPath, SDL_Renderer *renderer) {
	// TODO Auto-generated constructor stub
	dest = new SDL_Rect();
	dest->x = 280;
	dest->y = 450;
	dest->w = width;
	dest->h = height;

	src = new SDL_Rect();
	src->x = 0;
	src->y = 0;
	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
}
void Base::draw(SDL_Renderer *renderer){
	SDL_RenderCopy(renderer, texture, src, dest);
}

bool Base::collisionWithTank(Tank *tank){
	SDL_Rect *destDW = this->dest;
	if((tank->dest->x < destDW->x + destDW->w+2) and (tank->dest->x + tank->dest->w+2 > destDW->x) and
			(tank->dest->y < destDW->y+destDW->h+2) and (tank->dest->y + tank->dest->h+2 > destDW->y) and
		 tank->state == tank->lastState){

			if(tank->state == 1){
				if(tank->dest->x == this->dest->x+this->dest->w){
					return false;
				}else{
					return true;
				}
			}else if(tank->state == 2){
				if(tank->dest->x+1 + tank->dest->w == this->dest->x){
					return true;
				}else{
					return false;
				}
			}else if(tank->state == 8){
				if(tank->dest->y + tank->dest->h == this->dest->y){
					return true;
				}else{
					return false;
				}
			}
		}
	return false;
}

bool Base::collisionWithProjectile(Projectile *projectile){
	SDL_Rect *destDW = projectile->dest;
		//	last = currentState;

	if((dest->x < destDW->x + destDW->w+2) and (dest->x + dest->w+2 > destDW->x) and
		(dest->y < destDW->y+destDW->h+2) and (dest->y + dest->h+2 > destDW->y)){

		return true;
	}
	return false;
}
Base::~Base() {
	// TODO Auto-generated destructor stub
	delete src;
	delete dest;
}

