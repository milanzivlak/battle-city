/*
 * destroyablewall.h
 *
 *  Created on: Jan 13, 2020
 *      Author: Zivlak
 */

#ifndef DESTROYABLEWALL_H_
#define DESTROYABLEWALL_H_
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"

#include <iostream>
using namespace std;
class DestroyableWall : public Drawable {
public:
	SDL_Rect *src;
	SDL_Rect *dest;
	SDL_Texture *texture;
	int width;
	int height;
	int hits = 0;
	DestroyableWall(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	void draw(SDL_Renderer * renderer);
	virtual ~DestroyableWall();
};

#endif /* DESTROYABLEWALL_H_ */
