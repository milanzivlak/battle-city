/*
 * projectile.cpp
 *
 *  Created on: Jan 25, 2020
 *      Author: Zivlak
 */

#include "projectile.h"

Projectile::Projectile(int width, int height, string spriteSheetPath, SDL_Renderer *renderer){
	// TODO Auto-generated constructor stub
	dest = new SDL_Rect();
	dest->x = 1000;
	dest->y = 1000;
	dest->w = width;
	dest->h = height;

	src = new SDL_Rect();
	src->x = 0;
	src->y = 0;
	src->w = width;
	src->h = height;

	SDL_Surface *surface = IMG_Load(spriteSheetPath.c_str());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

}

void Projectile::move(int dx, int dy){
	dest->x += dx;
	dest->y += dy;
}
void Projectile::move(){
	/*
	 * Move za projektile je uradjen da se kreze brze od tenkova.
	 */

	if(state == LEFT){
		move(-4, 0);
	}else if(state == RIGHT){
		move(4, 0);
	}else if(state == UP){
		move(0, -4);
	}else if(state == DOWN){
		move(0, 4);
	}
}
void Projectile::draw(SDL_Renderer *renderer){
	if(state == UP){
		src->x = 0;
	}else if(state == LEFT){
		src->x = 16;
	}else if(state == DOWN){
		src->x = 32;
	}else if(state == RIGHT){
		src->x = 48;
	}
	SDL_RenderCopy(renderer, texture, src, dest);
}
bool Projectile::dWallCollision(DestroyableWall *dw){
	/*
	 * Ispitujemo koliziju projektila sa zidom koji je unistiv.
	 */

	SDL_Rect *destDW = dw->dest;

	if((dest->x < destDW->x + destDW->w) and (dest->x + dest->w > destDW->x) and
		(dest->y < destDW->y+destDW->h) and (dest->y + dest->h > destDW->y)){

		return true;
	}
	return false;
}


Projectile::~Projectile() {
	// TODO Auto-generated destructor stub
	delete dest;
	delete src;
}

