/*
 * tank.h
 *
 *  Created on: Dec 26, 2019
 *      Author: Zivlak
 */

#ifndef TANK_H_
#define TANK_H_
#include <SDL2/SDL.h>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"
#include "destroyablewall.h"
#include "projectile.h"
#include <vector>
#include <iostream>
using namespace std;
class Projectile;
class Tank : public Drawable, public Movable, public KeyboardEventListener {
public:
	enum State:short int {STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8, SHOOT = 10,
	                     LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
	                     RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};

	short int state;
	short int lastState; //Namjenjeno za poredjenje radi kretanja tenka nakon kolizije
	short int nextState;
	short int currentState;
	short int last = 0;
	int nextX;
	int nextY;
	int forwardX;
	int forwardY;
	int width;
	int height;
	int x;
	int y;
	int currentFrame = 0;
	int frameCounter = 0;
	SDL_Texture *texture;
	SDL_Rect *src;
	SDL_Rect *dest;
	vector<Projectile*> projectiles;
	Projectile *projectile;
	SDL_Renderer* noviRend;
	Tank(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	Tank(istream &inputStream);
	virtual void draw(SDL_Renderer *renderer);
	void drawFrame(SDL_Rect *src, SDL_Rect *dest, SDL_Renderer *renderer);
	virtual void move(int dx, int dy);
    virtual void move();
    virtual void listen(SDL_KeyboardEvent &event);
    bool collision(DestroyableWall *dw);
    bool collisionWithProjectile(Projectile *p);
    void shoot(SDL_Renderer *renderer);

	virtual ~Tank();
};

#endif /* TANK_H_ */
