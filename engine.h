#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "drawable.h"
#include "movable.h"
#include "tileset.h"
#include "level.h"
#include "tank.h"

#include "eventlistener.h"

#include "enemy.h"
#include "destroyablewall.h"
#include "base.h"
#include "projectile.h"
#include "field.h"
#include "table.h"
#include "score.h"
using namespace std;


class Engine {
private:
    map<string, Tileset*> tilesets;
    vector<Drawable*> drawables;
    vector<Movable*> movables;
    vector<EventListener*> eventListeners;
    SDL_Window *window;
    SDL_Renderer *renderer;
    int start = 0;
    int end;
    int accumulator = 0;
    int startShooting = 0;
    int endShooting;
    int shootingAccumulator = 0;
    int frameCap = 60;
public:

    Engine(string title);

    void addTileset(Tileset *tileset, const string &name);

    void addTileset(istream &inputStream, const string &name);

    void addTileset(const string &path, const string &name);

    Tileset* getTileset(const string &name);


    void addDrawable(Drawable* drawable);

    void run();

    virtual ~Engine();
};

#endif // ENGINE_H_INCLUDED
