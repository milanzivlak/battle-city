
#include <iostream>
#include <fstream>

#include "engine.h"
#include "level.h"


using namespace std;


int main(int argc, char** argv)
{
    Engine *eng = new Engine("Tanks");
    eng->addTileset("resources/tilesets/wall_tileset.txt", "default");

    ifstream levelStream("resources/levels/level2.txt");
    eng->addDrawable(new Level(levelStream, eng->getTileset("default")));

    eng->run();
    delete eng;
    return 0;
}
