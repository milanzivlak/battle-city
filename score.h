/*
 * score.h
 *
 *  Created on: Mar 16, 2020
 *      Author: Zivlak
 */

#ifndef SCORE_H_
#define SCORE_H_
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"

#include <iostream>
using namespace std;
class Score : public Drawable{
public:
	int tableScore = 0;
	SDL_Rect *src;
	SDL_Rect *dest;
	SDL_Texture *texture;
	int width;
	int height;
	Score(int width, int height, string spriteSheetPath, SDL_Renderer *renderer);
	void draw(SDL_Renderer * renderer);
	virtual ~Score();
};

#endif /* SCORE_H_ */
